# DigioProducts

# Desafio:
- Utilizar JSON para fazer parse do nosso conteúdo. Seu desafio é muito simples: Você deve realizar parse desta url (https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products) para construir a tela a seguir.

## Requisitos:

- iOS 11.0+
- Xcode 9.4.1
- Swift 4.1

# Instalação
Para executar o projeto, é necessário ter o Cocoapods instalado:<br />
- sudo gem install cocoapods <br />
- pod install <br />
- Abrir o projeto através do arquivo VacationPlanner.xcworkspace<br /><br />