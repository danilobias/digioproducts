//
//  ProductsViewModel.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 08/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import Foundation

enum ProductType : Int {
    case spotlight = 0
    case cash = 1
    case product = 2
}

protocol ProductsViewModelProtocol: ListProtocol {
    var response: DigioResponse? { get }
    var responseDidChange: ((ProductsViewModelProtocol) -> Void)? { get set }
}

class ProductsViewModel: ProductsViewModelProtocol {
    
    // MARK: - Vars
    var response: DigioResponse? {
        didSet{
            self.responseDidChange?(self)
        }
    }
    
    var responseDidChange: ((ProductsViewModelProtocol) -> Void)?
    
    // MARK: - Methods
    required init() {}
    
    
    // MARK: - Utils
    func numberOfRows() -> Int {
        return response?.products?.count ?? 0
    }
    
    func getCash() -> GenericProduct? {
        return response?.cash
    }
    
    func getProductsByType(type: ProductType) -> [GenericProduct] {
        switch type {
        case .spotlight:
            return response?.spotlight ?? []
        case .cash:
            if let cash = self.getCash() {
                let cashArray: [GenericProduct] = [cash]
                return cashArray
            }
            
            return []
        case .product:
            return response?.products ?? []
        }
    }
    
    // MARK: - Request
    func getElement(completion: @escaping (Error?) -> Void) {
        let url: String = Constants.APIUrls.getProductsUrl
        ProductsRequest.getProducts(withURL: url) { (response, error) in
            if let digioResponse = response {
                self.response = digioResponse
            }
            
            if let errorDetail = error {
                completion(errorDetail)
            }
        }
    }
}
