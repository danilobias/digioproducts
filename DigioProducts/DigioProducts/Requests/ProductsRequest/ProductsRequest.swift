//
//  ProductsRequest.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 07/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductsRequest: NSObject {

    static func getProducts(withURL url: String, completion: @escaping(DigioResponse?, Error?) -> Void) {
        BaseRequest.get(url) { (result) in
            if let data = result as? Data {
                
                let json: JSON = JSON(data)
                let genreResponse: DigioResponse = DigioResponse(json: json)
                completion(genreResponse, nil)
                
            }else if let error = result as? Error {
                completion(nil, error)
            }else{
                completion(nil, ErrorManager.error(type: .unknown))
            }
        }
    }

}
