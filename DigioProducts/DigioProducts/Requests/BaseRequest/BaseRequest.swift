//
//  BaseRequest.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 07/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import Alamofire

class BaseRequest: NSObject {

    static let basicHeaders = [
        "accept-language": "en",
        "cache-control": "no-cache"
    ]
    
    static func get(_ url: String, _ parameters: Dictionary<String, Any>? = nil,  completion: @escaping(Any?) -> Void) {
        request(url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: basicHeaders).validate(statusCode: 200..<300).responseJSON { (response) in
            print("GET: \(response.request?.url?.absoluteString ?? "")")
            
            //            print(response.request)  // original URL request
            //            print(response.response) // HTTP URL response
            //            print(response.data)     // server data
            //            print(response.result)   // result of response serialization
            //            print("Error: \(response.result.error)")
            
            switch response.result{
            case .success:
                completion(response.data)
                break
                
            case .failure(let error):
                completion(error)
                break
            }
        }
    }
}
