//
//  ProductCollectionViewCell.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 10/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    
    // MARK: - Let's and VAR's
    let placeholderImage = UIImage(named: Constants.Placeholders.productPlaceholder)!
    
    // MARK: - Default
    override func prepareForReuse() {
        super.prepareForReuse()
//        self.productLabel.text = "-"
        self.productImage.image = placeholderImage
        self.productImage.contentMode = .center
    }
    
    // MARK: - Layout configs
    func configCellWith(product: GenericProduct) {
//        self.productLabel.text = product.name
        if let url = product.imageURL {
//            self.productImage.contentMode = .scaleAspectFit
            self.productImage.load(urlString: url, placeholder: Constants.Placeholders.productPlaceholder)
        }
    }
}
