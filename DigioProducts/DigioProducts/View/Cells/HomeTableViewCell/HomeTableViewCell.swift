//
//  HomeTableViewCell.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 09/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    // MARK: - Lets and Vars
    var products: [GenericProduct]!
    
    // MARK: - TableViewCell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - Utils
    func getProductBy(index: Int) -> GenericProduct {
        return self.products[index]
    }
    
    // MARK: - Layout configs
    func configCellWith(productsArray: [GenericProduct]) {
        self.products = productsArray
        self.productsCollectionView.reloadData()
    }
}

extension HomeTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        let genericProduct: GenericProduct = self.getProductBy(index: indexPath.row)
        cell.configCellWith(product: genericProduct)
        return cell
    }
}



