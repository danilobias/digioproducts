//
//  GlobalConstants.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 08/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import Foundation

struct Constants {
    
    //MARK: URL's e métodos
    struct APIPreffix {
        static let urlPreffix: String = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox"
    }
    
    struct URLPaths {
        static let commonPath = "/"
        static let concatKey = "&"
    }
    
    struct ProductMethods {
        static let getAllMethod = "/products"
    }
    
    struct APIUrls {
        static let getProductsUrl = Constants.APIPreffix.urlPreffix + Constants.URLPaths.commonPath + ProductMethods.getAllMethod
    }
    
    struct Placeholders {
        static let productPlaceholder: String = "digio"
    }

}
