//
//  HomeViewController.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 09/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var  productsTableView: UITableView!
    
    // MARK: - Lets and Vars
    var productsViewModel: ProductsViewModel! {
        didSet {
            productsViewModel.responseDidChange = { [weak self] viewModel in
                self?.showProducts()
            }
        }
    }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.productsViewModel = ProductsViewModel()
        self.makeGenresRequest()
    }
    
    // MARK: - Requests
    func makeGenresRequest() {
        self.showLoading()
        self.productsViewModel.getElement(completion: { (error) in
            // TO-DO: Tratar erro
            self.hideLoading()
        })
    }
    
    func showProducts() {
        self.hideLoading()
        self.productsTableView.reloadData()
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    // MARK: - Memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HomeViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        let productByType: [GenericProduct] = self.productsViewModel.getProductsByType(type: ProductType(rawValue: indexPath.section)!)
        cell.configCellWith(productsArray: productByType)
        cell.productsCollectionView.tag = indexPath.section
        return cell
    }
    
}

extension HomeTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag !=  2 {
            let itemsPerRow: CGFloat = 1
            let hardCodedPadding: CGFloat = 10
            let itemWidth: CGFloat = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
            let itemHeight: CGFloat = itemWidth
            return CGSize(width: itemWidth, height: itemHeight)
        }
        
        return CGSize(width: 135, height: 135)
    }
}

