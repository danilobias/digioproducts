//
//  GenericProduct.swift
//  DigioProducts
//
//  Created by Danilo Bias Lago on 10/11/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import SwiftyJSON

public struct GenericProduct {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let imageURL = "imageURL"
        static let name = "name"
        static let bannerURL = "bannerURL"
        static let title = "title"
    }
    
    // MARK: Properties
    public var imageURL: String?
    public var name: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        imageURL = json[SerializationKeys.imageURL].string
        name = json[SerializationKeys.name].string
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(spotlightJson: JSON) {
        name = spotlightJson[SerializationKeys.name].string
        imageURL = spotlightJson[SerializationKeys.bannerURL].string
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(cashJson: JSON) {
        name = cashJson[SerializationKeys.title].string
        imageURL = cashJson[SerializationKeys.bannerURL].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = imageURL { dictionary[SerializationKeys.imageURL] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        return dictionary
    }
}
