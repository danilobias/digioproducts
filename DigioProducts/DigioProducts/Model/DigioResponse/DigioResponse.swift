//
//  DigioResponse.swift
//
//  Created by Danilo Bias Lago on 07/11/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct DigioResponse {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let spotlight = "spotlight"
        static let products = "products"
        static let cash = "cash"
    }
    
    // MARK: Properties
    public var spotlight: [GenericProduct]?
    public var products: [GenericProduct]?
    public var cash: GenericProduct?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) {
        if let items = json[SerializationKeys.spotlight].array { spotlight = items.map { GenericProduct(json: $0) } }
        if let items = json[SerializationKeys.products].array { products = items.map { GenericProduct(json: $0) } }
        cash = GenericProduct(cashJson: json[SerializationKeys.cash])
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = spotlight { dictionary[SerializationKeys.spotlight] = value.map { $0.dictionaryRepresentation() } }
        if let value = products { dictionary[SerializationKeys.products] = value.map { $0.dictionaryRepresentation() } }
        if let value = cash { dictionary[SerializationKeys.cash] = value.dictionaryRepresentation() }
        return dictionary
    }
    
}
